const generateNumber = () => {
  return Math.round(Math.random() * 100);
};

const checkResult = (guessNumber, generatedNumber) => {
  if (guessNumber > generatedNumber) {
    showMessage("Số nhập vào lớn hơn số cần tìm");
    return false;
  } else if (guessNumber < generatedNumber) {
    showMessage("Số nhập vào nhỏ hơn số cần tìm");
    return false;
  } else {
    showMessage("Chúc mừng bạn đã đoán đúng số");
    return true;
  }
};

const showMessage = (message) => {
  document.querySelector("#message").innerHTML = message;
  document.querySelector("#message").classList.remove("d-none");
};

const renderGuessHistory = () => {
  let htmlContent = "";

  if (!guessHistory.length) {
    htmlContent = "<p><i>Chưa có lịch sử đoán</i></p>";
  }

  if (guessHistory.length) {
    for (const i in guessHistory) {
      htmlContent += `<p>Lần ${+i + 1}: bạn đã đoán số <strong>${
        guessHistory[i]
      }</strong></p>`;
    }
  }

  document.querySelector("#resultTable").innerHTML = htmlContent;
};

const deleteGuessHistory = () => {
  guessHistory = [];
  renderGuessHistory();
};

// validations
const checkRequired = (value, selector) => {
  if (value.trim().length > 0) {
    document.querySelector(selector).classList.remove("is-invalid");
    document.querySelector(`${selector} + .invalid-feedback`).innerHTML = "";
    return true;
  }

  document.querySelector(selector).classList.add("is-invalid");
  document.querySelector(`${selector} + .invalid-feedback`).innerHTML =
    "Bạn chưa nhập trường này";
  return false;
};

const checkNumber = (value, selector) => {
  const pattern = /^\-?[0-9]+$/g;
  if (pattern.test(value)) {
    document.querySelector(selector).classList.remove("is-invalid");
    document.querySelector(`${selector} + .invalid-feedback`).innerHTML = "";
    return true;
  }

  document.querySelector(selector).classList.add("is-invalid");
  document.querySelector(`${selector} + .invalid-feedback`).innerHTML =
    "Giá trị nhập vào phải là số";
  return false;
};

const checkNumberValue = (min, max, value, selector) => {
  if (value >= min && value <= max) {
    document.querySelector(selector).classList.remove("is-invalid");
    document.querySelector(`${selector} + .invalid-feedback`).innerHTML = "";
    return true;
  }

  document.querySelector(selector).classList.add("is-invalid");
  document.querySelector(
    `${selector} + .invalid-feedback`
  ).innerHTML = `Giá trị nhập vào phải từ ${min} đến ${max}`;
  return false;
};

// generate a random number
let generatedNumber = generateNumber();

let guessHistory = [];

renderGuessHistory();

// main
document.querySelector("#btnGuess").addEventListener("click", () => {
  const guessTxt = document.querySelector("#txtNumber");

  // get entered number
  const guessNumber = guessTxt.value;

  // validate form
  if (
    !checkRequired(guessNumber, "#txtNumber") ||
    !checkNumber(guessNumber, "#txtNumber") ||
    !checkNumberValue(0, 100, +guessNumber, "#txtNumber")
  )
    return;

  // check result
  const result = checkResult(guessNumber, generatedNumber);

  guessHistory.push(guessNumber);

  if (result) {
    // print generated number to UI
    document.querySelector("#generatedNumber").innerHTML = generatedNumber;

    renderGuessHistory();

    guessTxt.setAttribute("disabled", true);
    document.querySelector("#btnGuess").classList.add("d-none");
    document.querySelector("#btnNewGame").classList.remove("d-none");
  }
});

const playNewGame = () => {
  const guessTxt = document.querySelector("#txtNumber");

  guessTxt.value = "";
  guessTxt.removeAttribute("disabled");
  document.querySelector("#generatedNumber").innerHTML = "--";
  document.querySelector("#btnGuess").classList.remove("d-none");
  document.querySelector("#btnNewGame").classList.add("d-none");

  deleteGuessHistory();

  // regenerate a number
  generatedNumber = generateNumber();
};
